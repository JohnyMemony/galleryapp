import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Image } from '../image';
import { ImageService } from '../services/image.service';
import { CategoriesService } from '../services/categories.service';
import { Category } from "../category";


@Component({
    selector: 'add-image',
    templateUrl: './add-image.component.html',
    styleUrls: [ 'add-image.component.css' ]
})

export class addImageComponent implements OnInit{

    image = {
        imageName: '',
        imageCategory: '',
        imageDescription: '',
        imageUrl: '',
    };

    categories: Category[];

    categoriesOptions:  Array<any> = [];

    images: Image[];

    constructor(
        private imageService: ImageService,
        private categoriesService: CategoriesService,
        private router: Router) { }

    ngOnInit(): void {
        this.categoriesService.getCategories()
            .then(categories => {
                this.categories = categories;

                for(let category of this.categories){

                    this.categoriesOptions.push(
                        {
                            value: category.name,
                            label: category.name
                        }
                    )

                }

                this.categoriesOptions = this.categoriesOptions.splice(0);

            })
    }

    add(name: string, url: string, category: string, description: string): void {
        if (!name) { return; }
        this.imageService.create(name, url, category, description)
            .then(() => {
                this.router.navigate(['/'])
            });
    }

    submit(){

        this.add(
            this.image.imageName,
            this.image.imageUrl,
            this.image.imageCategory,
            this.image.imageDescription
        );

        this.imageService.getImages()
            .then(images => {
                this.images = images;
            });

    }

    onSingleSelected(item) {
        this.image.imageCategory = item.value;
    }


//http://wallpaperscraft.ru/image/podvesnoj_most_most_derevya_115524_1680x1050.jpg
}