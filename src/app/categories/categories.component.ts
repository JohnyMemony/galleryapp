import {Component, EventEmitter, Output, OnInit} from '@angular/core';

import { Category } from '../category';
import { CategoriesService } from '../services/categories.service';

@Component({
    selector: 'categories',
    templateUrl: './categories.component.html',
    styleUrls: [ 'categories.component.css' ]
})

export class CategoriesListComponent implements OnInit{
    categories: Category[] = [];

    @Output() chosenCategory: EventEmitter<string> = new EventEmitter<string>();

    constructor(private CategoriesService: CategoriesService) {}

    ngOnInit(): void {
        this.CategoriesService.getCategories()
            .then(categories => this.categories = categories);
    }

    /**
     * Switching current image list category
     * @param event
     * @param activeClassName
     * @param buttonSelector
     */
    choiceCategory(event, activeClassName, buttonSelector) {
        let clickedButton = event.target,
            buttons = document.querySelectorAll(buttonSelector);

        for(let button of buttons){
            button.classList.remove(activeClassName);
        }

        clickedButton.classList.add(activeClassName);

        this.chosenCategory.emit(clickedButton.innerHTML.trim());

    }

}