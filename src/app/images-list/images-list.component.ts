import { Component, OnInit } from '@angular/core';

import { Image } from '../image';
import { ImageService } from '../services/image.service';
import { IMAGES_PATH } from '../globals'

@Component({
    selector: 'images-list',
    templateUrl: './images-list.component.html',
    styleUrls: [ 'images-list.component.css' ]
})

export class ImagesListComponent implements OnInit{
    imagesRootPath: string;
    images: Image[] = [];
    currentCategory = 'all';
    
    constructor(private imageService: ImageService) {

        this.imagesRootPath = IMAGES_PATH;

    }

    ngOnInit(): void {

        this.imageService.getImages()
            .then(images => this.images = images);

    }
    
    onCategoryChoice(category: string): void {

        this.currentCategory = category;

    }

    delete(image: Image): void {
        this.imageService
            .delete(image.id)
            .then(() => {
                this.images = this.images.filter(h => h !== image);
            });
    }

}

