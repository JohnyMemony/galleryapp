"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ImagesList = (function () {
    function ImagesList(imageService) {
        this.imageService = imageService;
        this.images = [];
        this.images = this.imageService.getImages();
        this.imagesRootPath = 'src/assets/images/';
    }
    return ImagesList;
}());
ImagesList = __decorate([
    core_1.Component({
        selector: 'images-list',
        templateUrl: './images-list.component.html'
    })
], ImagesList);
exports.ImagesList = ImagesList;
