export class Image {

    constructor(
       public id: number,
       public name: string,
       public fileName: string,
       public category: string,
       public url?: string,
       public description?: string,
       public resolution?: string,
    ) { }

}