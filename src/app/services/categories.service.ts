import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Category } from "../category";

@Injectable()
export class CategoriesService {

    constructor(private http: Http) { }

    private imagesUrl = 'api/categories';

    getCategories(): Promise<Category[]> {
        return this.http.get(this.imagesUrl)
            .toPromise()
            .then(response => response.json().data as Category[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}