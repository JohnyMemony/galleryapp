import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Image } from "../image";

@Injectable()
export class ImageService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private imagesUrl = 'api/images';

    constructor(private http: Http) { }

    getImages(): Promise<Image[]> {
        return this.http.get(this.imagesUrl)
            .toPromise()
            .then(response => response.json().data as Image[])
            .catch(this.handleError);
    }

    getImage(id: number): Promise<Image> {
        const url = `${this.imagesUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as Image)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.imagesUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    create(name: string, url: string, category: string, description: string): Promise<Image> {
        return this.http
            .post(this.imagesUrl, JSON.stringify({
                name: name,
                url: url,
                category: category,
                description: description,
            }), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data as Image)
            .catch(this.handleError);
    }

    update(image: Image): Promise<Image> {
        const url = `${this.imagesUrl}/${image.id}`;
        return this.http
            .put(url, JSON.stringify(image), {headers: this.headers})
            .toPromise()
            .then(() => image)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}