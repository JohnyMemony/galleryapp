import  { Pipe, PipeTransform } from "@angular/core";
import {Image} from "../image";

@Pipe({
    name: 'sortByCategory'
})

export class sortByCategory implements PipeTransform {
    transform(array: Array<Image>, category: string): Array<Image> {

        if (category === 'all'){
            return array;
        }

        array = array.filter(function (image) {
            return image.category === category
        });

        return array;
    }
}