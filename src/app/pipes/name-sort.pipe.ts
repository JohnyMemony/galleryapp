import  { Pipe, PipeTransform } from "@angular/core";
import {Image} from "../image";

@Pipe({
    name: 'sortByName'
})

export class sortByName implements PipeTransform {
    transform(array: Array<Image>, name: string): Array<Image> {

        let pattern = new RegExp(name, 'i');

        array = array.filter(function (image) {
            return image.name.search(pattern) != -1
        });

        return array;
    }
}