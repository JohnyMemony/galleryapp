import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let images = [
            {
                id: 1,
                name: 'Mechanism',
                fileName: 'mehanizm_fon_linii_ustroystvo_74820_1680x1050.jpg',
                category: '3d',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, uis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 2,
                name: 'Metallic mechanism',
                fileName: 'mehanizm_metall_ustroystvo_10404_1680x1050.jpg',
                category: '3d',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetuveniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 3,
                name: 'Gear',
                fileName: 'shesterenki_pruzhina_mehanizm_mehanika_76547_1680x1050.jpg',
                category: '3d',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet,  fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 4,
                name: 'Mechanic system',
                fileName: 'sistema_mehanizm_ustroystvo_chb_vzryv_10588_1680x1050.jpg',
                category: '3d',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
            {
                id: 5,
                name: 'Device',
                fileName: 'ustroystvo_mehanizm_seryy_dinamika_10627_1680x1050.jpg',
                category: '3d',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscingniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 6,
                name: 'Striped cat',
                fileName: 'kot_polosatyj_vzglyad_udivlenie_lezhat_94958_1680x1050.jpg',
                category: 'animals',
                resolution: '1680x1050',
                description: 'Lorem ipsore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 7,
                name: 'Cat',
                fileName: 'kot_polosatyy_lezhat_igrivyy_52921_1680x1050.jpg',
                category: 'animals',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 8,
                name: 'Red panda',
                fileName: 'krasnaya_panda_lazat_zverek_sherst_56285_1680x1050.jpg',
                category: 'animals',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 9,
                name: 'Little red panda',
                fileName: 'krasnaya_panda_malaya_panda_derevo_lazat_114894_1680x1050.jpg',
                category: 'animals',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 10,
                name: 'Ferrari 458',
                fileName: '458_italia_sea_ferrari_tuning_zheltyy_76849_1680x1050.jpg',
                category: 'cars',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amiqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 11,
                name: 'Ferrari 458 (back view)',
                fileName: 'ferrari_458_vid_sboku_108196_1680x1050.jpg',
                category: 'cars',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 12,
                name: 'Ferrari',
                fileName: 'ferrari_458_vid_szadi_bamper_96354_1680x1050.jpg',
                category: 'cars',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 13,
                name: 'City',
                fileName: 'gorod_chb_doroga_ulica_noch_dvizhenie_hdr_10860_1680x1050.jpg',
                category: 'cites',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetunim id est laborum.'
            },
            {
                id: 14,
                name: 'Christmas city',
                fileName: 'gorod_ulica_prazdnik_rozhdestvo_atmosfera_hdr_54830_1680x1050.jpg',
                category: 'cites',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 15,
                name: 'Night city',
                fileName: 'ulica_gorod_noch_derevo_hdr_28556_1680x1050.jpg',
                category: 'cites',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 16,
                name: 'City lights',
                fileName: 'ulica_gorod_vecher_chb_fonari_zdaniya_hdr_25485_1680x1050.jpg',
                category: 'cites',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 17,
                name: 'Cherries',
                fileName: 'chereshnya_krasnyy_vetka_spelyy_yagoda_4257_1680x1050.jpg',
                category: 'food',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 18,
                name: 'Raspberry',
                fileName: 'malina_krasnyy_yagoda_vetka_spelyy_listya_zelenyy_4251_1680x1050.jpg',
                category: 'food',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 19,
                name: 'grapes',
                fileName: 'vinograd_kist_vetka_listya_spelyy_17517_1680x1050.jpg',
                category: 'food',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 20,
                name: 'Raspberry',
                fileName: 'yagody_malina_vetka_zelenyy_trava_listya_17354_1680x1050.jpg',
                category: 'food',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 21,
                name: 'Blob',
                fileName: 'cvety_ld_makro_kapli_73848_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 22,
                name: 'Orange',
                fileName: 'makro_apelsin_zelenyy_kapli_4376_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 23,
                name: 'Water',
                fileName: 'makro_kapli_voda_list_zelenyy_4437_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 24,
                name: 'Leaf',
                fileName: 'makro_list_osen_suhoy_skryuchennyy_73717_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 25,
                name: 'Snow',
                fileName: 'makro_sneg_trava_suhoy_led_4547_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 26,
                name: 'Snowflake',
                fileName: 'snezhinka_zima_makro_led_98434_1680x1050.jpg',
                category: 'macro',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 27,
                name: 'lake',
                fileName: 'les_ozero_otrazhenie_ostrov_tuman_97668_1680x1050.jpg',
                category: 'nature',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 28,
                name: 'Trail',
                fileName: 'les_tropa_kamni_derevya_osen_listya_7116_1680x1050.jpg',
                category: 'nature',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 29,
                name: 'Forrest fog',
                fileName: 'les_tuman_utro_gory_otrazhenie_cveta_46065_1680x1050.jpg',
                category: 'nature',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 30,
                name: 'Space',
                fileName: 'kosmos_planeta_zvezdy_ten_62610_1680x1050.jpg',
                category: 'space',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 31,
                name: 'Eclipse',
                fileName: 'lunnoe_zatmenie_solnechnoe_zatmenie_kosmos_97617_1680x1050.jpg',
                category: 'space',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 32,
                name: 'Milky way',
                fileName: 'mlechnyj_put_zvezdy_noch_nebo_kosmos_97654_1680x1050.jpg',
                category: 'space',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 33,
                name: 'Stars',
                fileName: 'planeta_svet_pyatna_kosmos_86643_1680x1050.jpg',
                category: 'space',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 34,
                name: 'Star sky',
                fileName: 'zvezdy_nebo_bereg_84534_1680x1050.jpg',
                category: 'space',
                resolution: '1680x1050',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }
        ];
        let categories = [
            {
                id: 2,
                name: 'animals'
            },
            {
                id: 1,
                name: '3d'
            },
            {
                id: 3,
                name: 'cars',
            },
            {
                id: 4,
                name: 'cites'
            },
            {
                id: 5,
                name: 'food'
            },
            {
                id: 6,
                name: 'macro'
            },
            {
                id: 7,
                name: 'nature'
            },
            {
                id: 8,
                name: 'space'
            }
        ];
        return {images, categories};
    }
}
