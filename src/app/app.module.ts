import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module'

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

//Modules
import {SelectModule} from 'angular2-select';

//Pipes
import { sortByCategory } from './pipes/category-sort.pipe'
import { sortByName } from './pipes/name-sort.pipe'

//Directives


//Services
import { ImageService } from './services/image.service'
import { CategoriesService } from "./services/categories.service";

//Components
import { AppComponent } from './app.component';
import { ImagesListComponent } from './images-list/images-list.component'
import { CategoriesListComponent } from "./categories/categories.component";
import { SearchComponent } from "./search/search.component";
import { ImageDetailComponent } from "./image-detail/image-detail.component";
import {addImageComponent } from  "./add-image/add-image.component"


@NgModule({
  declarations: [
    AppComponent,
    sortByCategory,
    sortByName,
    ImagesListComponent,
    CategoriesListComponent,
    SearchComponent,
    ImageDetailComponent,
    addImageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    SelectModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
  ],
  providers: [ImageService, CategoriesService],
  bootstrap: [AppComponent]
})

export class AppModule { }
