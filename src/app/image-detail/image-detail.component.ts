import 'rxjs/add/operator/switchMap';
import {Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Image } from 'app/image'
import { ImageService } from 'app/services/image.service'
import { IMAGES_PATH } from '../globals'

@Component({
    selector: 'images-detail',
    templateUrl: 'image-detail.component.html',
    styleUrls: [ 'image-detail.component.css' ]
})

export class ImageDetailComponent implements OnInit{

    image: Image;
    imagesRootPath: string;
    changesSaved = false;

    @ViewChild('descriptionContainer') descriptionContainer;
    @ViewChild('nameContainer') nameContainer;
    @ViewChild('saveBtn') saveBtn;

    constructor(
        private imageService: ImageService,
        private route: ActivatedRoute,
        private renderer: Renderer2
    ){

        this.imagesRootPath = IMAGES_PATH;

    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.imageService.getImage(+params['id']))
            .subscribe(image => { this.image = image; });
    }


    /**
     * Makes editable image description and image name
     */
    editContent(){

        this.changesSaved = false;

        this.renderer.setAttribute(this.nameContainer.nativeElement, 'contenteditable', 'true');
        this.renderer.setAttribute(this.descriptionContainer.nativeElement, 'contenteditable', 'true');

        this.renderer.addClass(this.nameContainer.nativeElement, 'editable');
        this.renderer.addClass(this.descriptionContainer.nativeElement, 'editable');

        this.renderer.removeAttribute(this.saveBtn.nativeElement, 'disabled');

    }

    /**
     * Save changes
     */
    imageUpdate(){

        this.image.name = this.nameContainer.nativeElement.innerHTML;
        this.image.description = this.descriptionContainer.nativeElement.innerHTML;

        this.imageService.update(this.image).then(() => {

            this.renderer.setAttribute(this.nameContainer.nativeElement, 'contenteditable', 'false');
            this.renderer.setAttribute(this.descriptionContainer.nativeElement, 'contenteditable', 'false');

            this.renderer.removeClass(this.nameContainer.nativeElement, 'editable');
            this.renderer.removeClass(this.descriptionContainer.nativeElement, 'editable');

            this.renderer.setAttribute(this.saveBtn.nativeElement, 'disabled', 'true');

            this.changesSaved = true;

        })

    }

}