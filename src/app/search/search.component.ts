import { Component, OnInit } from '@angular/core';
import { IMAGES_PATH } from '../globals'

import { Image } from '../image';
import { ImageService } from '../services/image.service';

@Component({
    selector: 'search',
    templateUrl: './search.component.html',
    styleUrls: [ 'search.component.css' ]
})

export class SearchComponent implements OnInit{
    imagesRootPath: string;
    images: Image[] = [];
    searchValue: string;

    constructor(private imageService: ImageService) {

        this.imagesRootPath = IMAGES_PATH;

    }

    ngOnInit(): void {
        this.imageService.getImages()
            .then(images => this.images = images);
    }

    search(){
        if(this.searchValue){

            console.log(this.searchValue);

        }
    }
}