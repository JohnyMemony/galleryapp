import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImagesListComponent }   from './images-list/images-list.component';
import { SearchComponent }      from './search/search.component';
import { ImageDetailComponent }  from './image-detail/image-detail.component';
import { addImageComponent } from './add-image/add-image.component';

const routes: Routes = [
    { path: '', redirectTo: '/list', pathMatch: 'full' },
    { path: 'list',  component: ImagesListComponent },
    { path: 'detail/:id', component: ImageDetailComponent },
    { path: 'search',     component: SearchComponent },
    { path: 'new',     component: addImageComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})


export class AppRoutingModule {}
